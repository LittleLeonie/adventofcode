
def _11_1():
    smatrix = []
    with open("d11/d11.input", "r") as f:
        for ln in f:
            ln = ln.rstrip()
            if ln and ln is not "-":
                row = []
                for char in ln:
                    char_bool = None
                    if char == "L":
                        char_bool = False
                    elif char == "#":
                        char_bool = True
                    row.append(char_bool)
                smatrix.append(row)
            else:
                def simulate_waiting_room(seat_matrix):
                    cardinals = {
                        "N": (0, -1),
                        "NE": (1, -1),
                        "E": (1, 0),
                        "SE": (1, 1),
                        "S": (0, 1),
                        "SW": (-1, 1),
                        "W": (-1, 0),
                        "NW": (-1, -1),
                    }

                    new_seat_matrix = []
                    for row in seat_matrix:
                        n_row = row.copy()
                        new_seat_matrix.append(n_row)

                    for row_index, row in enumerate(seat_matrix):
                        for seat_index, seat in enumerate(row):
                            if seat is not None:
                                check_list = []
                                for direction, coordinates in cardinals.items():
                                    x = coordinates[0]
                                    y = coordinates[1]
                                    if not row_index + y < 0 and not seat_index + x < 0:
                                        try: check_list.append(seat_matrix[row_index + y][seat_index + x])
                                        except IndexError: pass

                                try:
                                    check_list.remove(None)
                                except ValueError:
                                    pass

                                if check_list.count(True) == 0:
                                    new_seat_matrix[row_index][seat_index] = True
                                elif check_list.count(True) >= 4:
                                    new_seat_matrix[row_index][seat_index] = False

                    occupied_seats = 0
                    for row in new_seat_matrix:
                        occupied_seats += row.count(True)

                    return seat_matrix, new_seat_matrix, occupied_seats

                test_matrix, new_matrix, occupied_seats = simulate_waiting_room(smatrix)
                while test_matrix != new_matrix:
                    test_matrix, new_matrix, occupied_seats = simulate_waiting_room(new_matrix)
                print(occupied_seats)


def _11_2():
    smatrix = []
    with open("d11/d11.input", "r") as f:
        for ln in f:
            ln = ln.rstrip()
            if ln and ln is not "-":
                row = []
                for char in ln:
                    char_bool = None
                    if char == "L":
                        char_bool = False
                    elif char == "#":
                        char_bool = True
                    row.append(char_bool)
                smatrix.append(row)
            else:
                def simulate_waiting_room(seat_matrix):
                    cardinals = {
                        "N": (0, -1),
                        "NE": (1, -1),
                        "E": (1, 0),
                        "SE": (1, 1),
                        "S": (0, 1),
                        "SW": (-1, 1),
                        "W": (-1, 0),
                        "NW": (-1, -1),
                    }

                    new_seat_matrix = []
                    for row in seat_matrix:
                        n_row = row.copy()
                        new_seat_matrix.append(n_row)

                    for row_index, row in enumerate(seat_matrix):
                        for seat_index, seat in enumerate(row):
                            if seat is not None:
                                check_list = []
                                for direction, coordinates in cardinals.items():
                                    x = coordinates[0]
                                    y = coordinates[1]
                                    tmp_row_index = row_index
                                    tmp_seat_index = seat_index
                                    while len(seat_matrix) >= tmp_row_index + y >= 0 and len(row) >= tmp_seat_index + x >= 0:
                                        try:
                                            if seat_matrix[tmp_row_index + y][tmp_seat_index + x] is not None:
                                                check_list.append(seat_matrix[tmp_row_index + y][tmp_seat_index + x])
                                                break
                                        except IndexError:
                                            pass
                                        tmp_row_index += y
                                        tmp_seat_index += x

                                try:
                                    check_list.remove(None)
                                except ValueError:
                                    pass

                                if check_list.count(True) == 0:
                                    new_seat_matrix[row_index][seat_index] = True
                                elif check_list.count(True) >= 5:
                                    new_seat_matrix[row_index][seat_index] = False

                    occupied_seats = 0
                    for row in new_seat_matrix:
                        occupied_seats += row.count(True)

                    return seat_matrix, new_seat_matrix, occupied_seats

                test_matrix, new_matrix, occupied_seats = simulate_waiting_room(smatrix)
                while test_matrix != new_matrix:
                    test_matrix, new_matrix, occupied_seats = simulate_waiting_room(new_matrix)
                print(occupied_seats)

