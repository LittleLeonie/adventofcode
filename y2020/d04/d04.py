import re


def _4_1():
    regex = re.compile(r"(\w{3}):([#\w]+)\s?", re.IGNORECASE)
    dict_keys = []
    required_keys = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
    validation_score = 0
    valid_passports = 0
    with open("d04/d04.input", "r") as f:
        for ln_index, ln in enumerate(f):
            ln = ln.rstrip()
            if ln and ln != "-":
                match = re.match(regex, ln)
                dict_keys.append(match.group(1))
            elif ln == "-" or not ln:
                for key in required_keys:
                    if key in dict_keys:
                        validation_score += 1

                if validation_score == len(required_keys):
                    valid_passports += 1
                validation_score = 0
                dict_keys = []
        print(valid_passports)


def _4_2():
    line_regex = re.compile(r"(\w{3}):([#\w]+)\s?", re.IGNORECASE)
    byr_regex = re.compile(r"^\d{4}$", re.IGNORECASE)
    iyr_regex = re.compile(r"^\d{4}$", re.IGNORECASE)
    eyr_regex = re.compile(r"^\d{4}$", re.IGNORECASE)
    hgt_regex = re.compile(r"^(\d{2,3})(cm|in)$", re.IGNORECASE)
    hcl_regex = re.compile(r"^#[0-9a-f]{6}$", re.IGNORECASE)
    ecl_regex = re.compile(r"(amb|blu|brn|gry|grn|hzl|oth)", re.IGNORECASE)
    pid_regex = re.compile(r"^\d{9}$", re.IGNORECASE)

    passport_dict = {}
    required_keys = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
    validation_score = 0
    valid_passports = 0
    with open("d04/d04.input", "r") as f:
        for ln_index, ln in enumerate(f):
            ln = ln.rstrip()
            if ln and ln != "-":
                match = re.match(line_regex, ln)
                if match.group(1) in passport_dict.keys():
                    continue
                passport_dict.update({
                    match.group(1): match.group(2)
                })
            elif ln == "-" or not ln:
                for key in required_keys:
                    if key in passport_dict.keys():
                        if key == "byr" and re.match(byr_regex, passport_dict[key]):
                            if 1920 <= int(passport_dict[key]) <= 2002:
                                validation_score += 1
                        elif key == "iyr" and re.match(iyr_regex, passport_dict[key]):
                            if 2010 <= int(passport_dict[key]) <= 2020:
                                validation_score += 1
                        elif key == "eyr" and re.match(eyr_regex, passport_dict[key]):
                            if 2020 <= int(passport_dict[key]) <= 2030:
                                validation_score += 1
                        elif key == "hgt" and re.match(hgt_regex, passport_dict[key]):
                            hgt_match = re.match(hgt_regex, passport_dict[key])
                            if hgt_match:
                                if hgt_match.group(2) == "cm" and 150 <= int(hgt_match.group(1)) <= 193:
                                    validation_score += 1
                                elif hgt_match.group(2) == "in" and 59 <= int(hgt_match.group(1)) <= 76:
                                    validation_score += 1
                        elif key == "hcl" and re.match(hcl_regex, passport_dict[key]):
                            validation_score += 1
                        elif key == "ecl" and re.match(ecl_regex, passport_dict[key]):
                            validation_score += 1
                        elif key == "pid" and re.match(pid_regex, passport_dict[key]):
                            validation_score += 1

                if validation_score == len(required_keys):
                    valid_passports += 1
                validation_score = 0
                passport_dict = {}
        print(valid_passports)
