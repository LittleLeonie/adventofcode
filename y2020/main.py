import re
import sys

from y2020.d01.d01 import *
from y2020.d02.d02 import *
from y2020.d03.d03 import *
from y2020.d04.d04 import *
from y2020.d05.d05 import *
from y2020.d06.d06 import *
from y2020.d07.d07 import *
from y2020.d08.d08 import *
from y2020.d09.d09 import *
from y2020.d10.d10 import *
from y2020.d11.d11 import *
from y2020.d12.d12 import *
from y2020.d13.d13 import *
from y2020.d14.d14 import *
from y2020.d15.d15 import *
from y2020.d16.d16 import *
from y2020.d17.d17 import *
from y2020.d18.d18 import *
from y2020.d19.d19 import *
from y2020.d20.d20 import *
from y2020.d21.d21 import *
from y2020.d22.d22 import *
from y2020.d23.d23 import *
from y2020.d24.d24 import *
from y2020.d25.d25 import *


# Functionality to execute methods by console input
if __name__ == '__main__':
    day_in = input("Of which day would you like to execute a puzzle?\n> ")
    puzzle_in = input(f"Which puzzle from day {day_in} of the calender would you like to execute?\n> ")
    day_in_import_format = f"d0{day_in}" if len(day_in) == 1 else f"d{day_in}"

    getattr(sys.modules[f"y2020.{day_in_import_format}.{day_in_import_format}"], f"_{day_in}_{puzzle_in}")()
