import re


def _2_1():
    ln_regex = re.compile(r"(\d+)-(\d+)\s(\w):\s(\w+)", re.IGNORECASE)
    valid = 0
    with open("d02/d02.input", "r", encoding="utf-8") as f:
        for ln in f:
            ln = ln.rstrip()
            if ln:
                ln_match = re.match(ln_regex, ln)
                char_min = int(ln_match.group(1))
                char_max = int(ln_match.group(2))
                target_char = ln_match.group(3)
                pw_string = ln_match.group(4)
                char_occasions = pw_string.count(target_char)

                if char_max >= char_occasions >= char_min:
                    valid += 1
    print(valid)


def _2_2():
    ln_regex = re.compile(r"(\d+)-(\d+)\s(\w):\s(\w+)", re.IGNORECASE)
    valid = 0
    with open("d02/d02.input", "r", encoding="utf-8") as f:
        for ln in f:
            ln = ln.rstrip()
            if ln:
                ln_match = re.match(ln_regex, ln)
                char_pos1 = int(ln_match.group(1))
                char_pos2 = int(ln_match.group(2))
                target_char = ln_match.group(3)
                pw_string = ln_match.group(4)
                if pw_string[char_pos1 - 1] == target_char:
                    if not pw_string[char_pos2 - 1] == target_char:
                        valid += 1
                elif pw_string[char_pos2 - 1] == target_char:
                    if not pw_string[char_pos1 - 1] == target_char:
                        valid += 1
    print(valid)
